﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace repliKate
{
    internal class MergeHandler
    {
        private string mCmExec;
        private bool mStopOnError;

        public MergeHandler(string cmExec, bool stopOnError)
        {
            mCmExec = cmExec;
            mStopOnError = stopOnError;
        }

        internal void AutoMerge(
            IDictionary<Branch, Changeset> mergeData, string workspacePath, 
            string srv, string pwd, int initBranch)
        {
            int init = Environment.TickCount;

            var rep = srv.Substring(0, srv.IndexOf('@'));
            var wkname = $"replikate-{rep}";

            var wkpath = GetWorkspacePath(pwd, wkname);
            if (wkpath == null)
            {
                wkpath = Path.Combine(workspacePath, rep);
                MakeWorkspace(pwd, wkname, wkpath, srv);
            }
            else
            {
                Log.InfoFormat($"Workspace {wkname} found at {wkpath}");
            }

            foreach (var pair in mergeData)
            {
                var branch = pair.Key;
                if (branch.Id < initBranch)
                {
                    continue;
                }

                var changeset = pair.Value;
                bool success;
                
                success = MergeBranch(wkpath, changeset.Id, branch.Name);
                if (!success)
                {
                    continue;
                }

                Log.InfoFormat("Done replicating branch {0}. {1} of {2}! Time so far {3}",
                                branch.Name, branch.Id, mergeData.Count,
                                TimeSpan.FromMilliseconds(Environment.TickCount - init).ToString());
            }
        }

        
        private string GetWorkspacePath(string pwd, string wkname)
        {
            var lwk = $@"{mCmExec} lwk --format=""{{0}}|{{2}}""";
            Cmd.Run(lwk, pwd);

            var rawinfo = Cmd.Output.Split('\n');
            var wkpath = (from wk in rawinfo
                          let split = wk.Split('|')
                          where split[0] == wkname
                          select split[1]).FirstOrDefault();

            return wkpath;
        }

        private bool MakeWorkspace(string pwd, string name, string path, string src)
        {
            return Checked($"make workspace {name} at path {path}", () =>
            {
                int ini = Environment.TickCount;

                var cmdresult = Cmd.Run($"{mCmExec} mkwk {name} {path} --repository={src}", pwd);

                Log.InfoFormat($"Created workspace {name} at {path} in {Environment.TickCount - ini} ms");

                if (cmdresult != 0)
                {
                    throw new Exception(Cmd.Error);
                }
            });
        }

        private bool MergeBranch(string pwd, long changeset, string branch)
        {
            return Checked($"merge changeset {changeset}", () => {
                int ini = Environment.TickCount;

                int cmdresult = Cmd.Run($"{mCmExec} merge cs:{changeset} --to=br:{branch} --merge", pwd);

                Log.InfoFormat($"Changeset {changeset} merged in {Environment.TickCount - ini} ms");

                if (cmdresult != 0)
                {
                    throw new Exception(Cmd.Error);
                }
            });
        }

        private bool Checked(string actionName, Action action)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                Log.ErrorFormat(
                    $"Failed to {actionName}:{Environment.NewLine}" +
                    $"{e.Message}{Environment.NewLine}" + 
                    $"at {e.StackTrace}");

                if (mStopOnError)
                {
                    throw e;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

    }
}
