# README #

RepliKate is a commandline tool provided by PlasticSCM on [their blog](http://blog.plasticscm.com/2012/02/after-accidentally-cloning-sexy.html).
Unfortunately the tool provided by PlasticSCM does not have the same feature set as the Sync View (Their GUI equivalent) which made the tool not suitable for my particular needs.

My version of replikate is built on top of the original with the following changes:

### Added new command line switches ###

replikate srcrepos@srcserver dstrepos@dstserver --mode=[pull|push|sync] --wkpath=path --xlinks=xlink1[,xlink2...]]

--mode
    pull: default mode, pulls changes from source to destination.
    push: pushes changes from soucre to destination instead of pulling from destination
    sync: pulls changes from destination, performs any necessary merges then pushes changes to destination
    
--wkpath
    sets the path to create a workspace named replikate-<repo> when on sync mode. 
    Note: I found the creation of this workspace necessary to perform the eventual fast-forward merge needed
    after pulling.
   
--xlinks
    a comma separated list of xlinks that should be replicated together with the main repo.

This way it's possible to have all repos related to a specific project to be synced using a single command (given that there will not be any conflicting changeset to be merged). For example:

`replikate myrepo@myorg@cloud myrepo@localserver:8087 --mode=sync --xlinks=game-module,art-module,sound-module`

### Restructured logging ###

On the original replikate, logging is done through Console.WriteLine despite the fact that the program makes use of log4net logging library to generate log files.

In this version I rewired how debuging messages are done. Now all debug messages relative to the replication process are streamed to log4net under the Debug level. Similarly, all actions are preceeded with a Log.Info entry. With this change one can simply tweak the RepliKate.log.conf file to filter Debug messages in or out to have a more or less verbose output respectively.
I also configured two different Console appenders on RepliKate.log.conf. One that spills colored console messages and another one that spills normal black and white messages to the console.
