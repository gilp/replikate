﻿using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;

namespace repliKate
{
    struct Branch
    {
        internal string Name;
        internal long Id;

        internal Branch(string name, string id)
        {
            Name = name;
            Id = long.Parse(id);
        }

        public override bool Equals(object obj)
        {
            return ((Branch)obj).Id == Id;
        }

        public override int GetHashCode()
        {
            return (int)Id;
        }

    }

    internal class BranchHandler
    {
        private string mSrcServer;
        private string mCmExec;
        private string mWkpath;

        public BranchHandler(string srcserver, string cmExec, string wkpath)
        {
            mSrcServer = srcserver;
            mCmExec = cmExec;
            mWkpath = wkpath;
        }

        internal IList<Branch> GetBranches()
        {
            var result = new List<Branch>();

            try
            {
                Cmd.Run($@"{mCmExec} find ""branch on repository '{mSrcServer}'"" " +
                        $@"--format={{name}}#{{id}} --nototal", mWkpath);

                StringReader reader = new StringReader(Cmd.Output);

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Trim() == string.Empty)
                        continue;

                    // output will be like /main/SCM5754#2625
                    string[] values = line.Split('#');

                    Branch br = new Branch(values[0], values[1]);
                    result.Add(br);
                }

                result.Sort(CompareBranches);
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Error listing branches:{0} at {1}{2}",
                    e.Message, Environment.NewLine, e.StackTrace);
                return null;
            }

            return result;
        }

        internal IList<Branch> FilterBranches(IList<Branch> branches, string initialdate)
        {
            string cmd = $@"{mCmExec} find ""changeset where date >= '{initialdate}' on repository '{mSrcServer}'"" " +
                         $@"--format={{branch}} --nototal";
            
            Cmd.Run(cmd, mWkpath);

            StringReader reader = new StringReader(Cmd.Output);

            ArrayList filtered = new ArrayList();

            // there will be many csets on some branches, only take on
            // branch for each

            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.Trim() == string.Empty)
                    continue;

                line = line.Trim();

                if (!filtered.Contains(line))
                    filtered.Add(line);
            }

            filtered.Sort();

            var result = new List<Branch>();
            foreach (string branchname in filtered)
            {
                // find it on the original list... (it has the id list)
                foreach (Branch br in branches)
                {
                    if (br.Name == branchname)
                    {
                        if (!result.Contains(br))
                            result.Add(br);
                        break;
                    }
                }
            }

            result.Sort(CompareBranches);

            return result;
        }

        private int CompareBranches(Branch x, Branch y)
        {
            return x.Id.CompareTo(y.Id);
        }
    }

}
