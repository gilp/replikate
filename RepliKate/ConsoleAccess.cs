﻿using System;

using Codice.CmdRunner;

namespace repliKate
{
    class ConsoleAccess : IConsoleWriter
    {
        public void WriteLine(string s)
        {
            Log.Debug(s);
        }
    }
}
