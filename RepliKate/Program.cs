﻿using System;

using System.Collections;
using System.Reflection;
using System.IO;
using Codice.CmdRunner;

using log4net;
using log4net.Config;
using System.Collections.Generic;

namespace repliKate
{
    class RepliKate
    {
        private static string INITIAL_BRANCH_ARG = "--initialbranch=";
        private static string SYNC_DATE_ARG = "--syncdate=";
        private static string CM_EXEC_ARG = "--cmexe=";
        private static string STOP_ON_ERROR_ARG = "--stoponerror=";
        private static string ONLY_SHOW_UNSYNCED_ARG = "--onlyshowunsynced=";
        private static string REPLICATE_MODE_ARG = "--mode=";
        private static string WORKSPACE_PATH_ARG = "--wkpath=";
        private static string XLINKS_ARG = "--xlinks=";

        static void Main(string[] args)
        {
            RepliKateParams rParams = GetRepliKateParams(args);
            if (rParams == null)
            {
                PrintUsage();
                return;
            }

            ConfigureLogging();

            InitCmdRunner();

            var start = DateTime.Now;
            Execute(rParams);
            var end = DateTime.Now;
            var time = end - start;

            Console.WriteLine($"\nReplication done in {time.ToString(@"hh\:mm\:ss\.ffff")}");
        }

        private static void Execute(RepliKateParams rParams)
        {
            Log.Info("Getting branch list...");
            var pullBranches = GetBranches(rParams);
            var pushBranches = GetBranches(rParams, true);

            var xlinkData = GetXLinkBranches(rParams);

            var mode = rParams.ReplicateMode;

            if (HasBranchesToReplicate(rParams.ReplicateMode, pullBranches, pushBranches, xlinkData))
            {
                PrintBranchesToReplicate(rParams.ReplicateMode, pullBranches, pushBranches, xlinkData);
            }
            else
            {
                var datemsg = string.IsNullOrEmpty(rParams.Syncdate) ? "" : $" since '{rParams.Syncdate}'";
                Log.Info($"No changeset found{datemsg}.");
                return;
            }

            if (rParams.OnlyShowUnsynced)
            {
                Log.Warn("Only showing branches to be synced. To perform the replication set --onlyshowunsynced to false.");
                return;
            }

            var replicator = new Replicator(rParams.CmExec, rParams.StopOnError);
            DoReplication(replicator, pullBranches, pushBranches, xlinkData, rParams);
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage: replikate srcrepos@srcserver dstrepos@dstserver" +
                "\n\t[--initialbranch=number]" +
                "\n\t[--syncdate=initialdate(Month/Day/Year) | yesterday]" +
                "\n\t[--cmexe=cm|bcm]" +
                "\n\t[--stoponerror=true|false] (Default: true) " +
                "\n\t[--onlyshowunsynced=true|false (Only shows unsynced branches. Default: false)]" +
                "\n\t[--mode=pull|push|sync (Replicate mode. Sync mode pulls first then pushes. Deault: pull)]" +
                "\n\t[--wkpath=path (Path to create temporary workspace to perform merge. Only used on sync mode. Default: '~/.replikate/wk')]" +
                "\n\t[--xlinks=xlink1[,xlink2...]] (xlinks to be replicated as well.)");
        }

        private static RepliKateParams GetRepliKateParams(string[] args)
        {
            if (args.Length < 2)
                return null;

            RepliKateParams rParams = new RepliKateParams();

            rParams.SrcSrv = args[0];
            rParams.DstSrv = args[1];

            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].StartsWith(INITIAL_BRANCH_ARG))
                {
                    rParams.InitBranch = int.Parse(args[i].Substring(
                        INITIAL_BRANCH_ARG.Length));

                    Log.Debug($"Going to use InitBranch:{rParams.InitBranch}");
                }
                else if (args[i].StartsWith(SYNC_DATE_ARG))
                {
                    rParams.Syncdate = args[i].Substring(SYNC_DATE_ARG.Length);
                    if (rParams.Syncdate.ToLower().Trim().StartsWith("yesterday"))
                        rParams.Syncdate = GetYesterdayDate();

                    Log.Debug($"Going to use SyncDate:{rParams.Syncdate}");
                }

                else if (args[i].StartsWith(CM_EXEC_ARG))
                {
                    rParams.CmExec = args[i].Substring(CM_EXEC_ARG.Length);
                    Log.Debug($"Going to use cm executable:{rParams.CmExec}");
                }
                else if (args[i].StartsWith(STOP_ON_ERROR_ARG))
                {
                    if (args[i].Substring(STOP_ON_ERROR_ARG.Length)
                        .ToLower().Trim().StartsWith("false"))
                    {
                        rParams.StopOnError = false;
                    }
                }
                else if (args[i].StartsWith(ONLY_SHOW_UNSYNCED_ARG))
                {
                    if (args[i].Substring(ONLY_SHOW_UNSYNCED_ARG.Length)
                        .ToLower().Trim().StartsWith("true"))
                    {
                        rParams.OnlyShowUnsynced = true;
                    }
                }
                else if (args[i].StartsWith(REPLICATE_MODE_ARG))
                {
                    var val = args[i].Substring(REPLICATE_MODE_ARG.Length).ToLower().Trim();
                    if (val.StartsWith("push"))
                    {
                        rParams.ReplicateMode = ReplicateMode.Push;
                    }
                    else if (val.StartsWith("pull"))
                    {
                        rParams.ReplicateMode = ReplicateMode.Pull;
                    }
                    else if (val.StartsWith("sync"))
                    {
                        rParams.ReplicateMode = ReplicateMode.Sync;
                    }
                    Log.Debug($"Going to operate on mode: {rParams.ReplicateMode}");
                }
                else if (args[i].StartsWith(WORKSPACE_PATH_ARG))
                {
                    rParams.WkPath = args[i].Substring(WORKSPACE_PATH_ARG.Length);
                    Log.Debug($"Going to use workspace directory: {rParams.WkPath}");
                }
                else if (args[i].StartsWith(XLINKS_ARG))
                {
                    rParams.XLinks = args[i].Substring(XLINKS_ARG.Length).Split(',');
                    Log.Debug($"Going to replicate the following xlinks: ({string.Join(", ", rParams.XLinks)})");
                }

                var isUnix = (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX);
                var homePath = isUnix
                    ? Environment.GetEnvironmentVariable("HOME")
                    : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");

                rParams.WkPath = Path.GetFullPath(rParams.WkPath.Replace("~", homePath));
            }

            return rParams;
        }

        private static void ConfigureLogging()
        {
            string log4netpath = Path.ChangeExtension(Assembly.GetExecutingAssembly().Location, ".log.conf");

            Console.WriteLine("Log at: " + log4netpath);
            XmlConfigurator.ConfigureAndWatch(new FileInfo(log4netpath));
        }

        private static void InitCmdRunner()
        {
            //CmdRunner.InitConsole(new ConsoleAccess());
            CmdRunner.SetBotWorkingMode();
        }

        private static IList<Branch> GetBranches(RepliKateParams rParams, bool push = false)
        {
            var srv = push ? rParams.DstSrv : rParams.SrcSrv;

            BranchHandler brHandler = new BranchHandler(
                srv, rParams.CmExec, Environment.CurrentDirectory);

            var branches = brHandler.GetBranches();

            if (branches == null) return null;

            if (rParams.Syncdate != string.Empty)
            {
                branches = brHandler.FilterBranches(branches, rParams.Syncdate);
            }

            var src = push ? rParams.SrcSrv : rParams.DstSrv;
            Log.Info($"Found {branches.Count} branches to {(push ? "push to" : "pull from")} {src}.");

            return branches;
        }

        private static List<XLinkData> GetXLinkBranches(RepliKateParams rParams)
        {
            var result = new List<XLinkData>();
            var srcSrv = rParams.SrcSrv.Substring(rParams.SrcSrv.IndexOf('@'));
            var dstSrv = rParams.DstSrv.Substring(rParams.DstSrv.IndexOf('@'));

            foreach (var xlink in rParams.XLinks)
            {
                var xSrcSrv = xlink + srcSrv;
                var xDstSrv = xlink + dstSrv;

                BranchHandler pullHandler = new BranchHandler(
                    xSrcSrv, rParams.CmExec, Environment.CurrentDirectory);
                BranchHandler pushHandler = new BranchHandler(
                    xDstSrv, rParams.CmExec, Environment.CurrentDirectory);


                Log.Info($"Getting branch list for xlink {xlink}...");
                var pullBranches = pullHandler.GetBranches();
                var pushBranches = pushHandler.GetBranches();

                if (pullBranches == null) return null;

                if (rParams.Syncdate != string.Empty)
                {
                    pullBranches = pullHandler.FilterBranches(pullBranches, rParams.Syncdate);
                    pushBranches = pushHandler.FilterBranches(pushBranches, rParams.Syncdate);
                }

                Log.Info($"Found {pullBranches.Count} branches to pull from {xDstSrv}.");
                Log.Info($"Found {pushBranches.Count} branches to push to {xSrcSrv}.");

                if (pullBranches.Count > 0 || pushBranches.Count > 0)
                {
                    result.Add(new XLinkData {
                        pullBranches = pullBranches,
                        pushBranches = pushBranches,
                        DstSrv = xDstSrv,
                        SrcSrv = xSrcSrv,
                        repo = xlink,
                    });
                }
            }

            return result;
        }

        private static bool HasBranchesToReplicate(ReplicateMode mode, IList<Branch> pullBranches, IList<Branch> pushBranches, IList<XLinkData> xlinkData)
        {
            if ((mode & ReplicateMode.Pull) != ReplicateMode.None)
            {
                if (pullBranches.Count > 0)
                {
                    return true;
                }

                foreach (var xlink in xlinkData)
                {
                    if (xlink.pullBranches.Count > 0)
                    {
                        return true;
                    }
                }
            }

            if ((mode & ReplicateMode.Push) != ReplicateMode.None)
            {
                if (pushBranches.Count > 0)
                {
                    return true;
                }

                foreach (var xlink in xlinkData)
                {
                    if (xlink.pushBranches.Count > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static void PrintBranchesToReplicate(ReplicateMode mode, IList<Branch> pullBranches, IList<Branch> pushBranches, IList<XLinkData> xlinkData)
        {
            Log.Info("Going to replicate the following branches:");

            if ((mode & ReplicateMode.Pull) != ReplicateMode.None)
            {
                foreach (Branch branch in pullBranches)
                {
                    Log.Info($"pull => br:{branch.Name} Id { branch.Id}");
                }

                foreach (var xlink in xlinkData)
                {
                    foreach (Branch branch in xlink.pullBranches)
                    {
                        Log.Info($"pull ({xlink.repo}) => br:{branch.Name} Id { branch.Id}");
                    }
                }
            }

            if ((mode & ReplicateMode.Push) != ReplicateMode.None)
            {
                foreach (Branch branch in pushBranches)
                {
                    Log.Info($"push => br:{branch.Name} Id { branch.Id}");
                }

                foreach (var xlink in xlinkData)
                {
                    foreach (Branch branch in xlink.pushBranches)
                    {
                        Log.Info($"push ({xlink.repo}) => br:{branch.Name} Id { branch.Id}");
                    }
                }
            }
        }

        private static void DoReplication(Replicator replicator, IList<Branch> pullBranches,
            IList<Branch> pushBranches, IList<XLinkData> xlinks, RepliKateParams rParams)
        {
            try
            {
                IDictionary<Branch, IList<Changeset>> initialChangesets = null;
                var changesetHandler = new ChangesetHandler(rParams.DstSrv, rParams.CmExec, Environment.CurrentDirectory);

                if (rParams.ReplicateMode == ReplicateMode.Sync)
                {
                    initialChangesets = changesetHandler.GetChangesets(pullBranches, rParams.Syncdate);
                }

                if ((rParams.ReplicateMode & ReplicateMode.Pull) != ReplicateMode.None)
                {
                    DoPullReplication(replicator, pullBranches, xlinks, rParams);
                }

                if (rParams.ReplicateMode == ReplicateMode.Sync)
                {
                    var diff = changesetHandler.GetChangesets(pullBranches, rParams.Syncdate, initialChangesets);
                    var mergeData = changesetHandler.GetLatestChangeset(diff);

                    if (mergeData.Count > 0)
                    {
                        DoAutomaticMerge(replicator, mergeData, rParams);
                    }
                }

                if ((rParams.ReplicateMode & ReplicateMode.Push) != ReplicateMode.None)
                {
                    DoPushReplication(replicator, pushBranches, xlinks, rParams);
                }
            }
            catch (Exception e)
            {
                Log.Error(
                    "Error replicating branches: " +
                    e.Message +
                    Environment.NewLine +
                    e.StackTrace);
            }
        }

        private static void DoPullReplication(Replicator replicator, IList<Branch> branches,
                IList<XLinkData> xlinks, RepliKateParams rParams)
        {
            replicator.Replicate(
                branches, rParams.SrcSrv, rParams.DstSrv,
                Environment.CurrentDirectory, rParams.InitBranch);

            foreach (var xlink in xlinks)
            {
                replicator.Replicate(
                    xlink.pullBranches, xlink.SrcSrv, xlink.DstSrv,
                    Environment.CurrentDirectory, rParams.InitBranch);
            }
        }

        private static void DoPushReplication(Replicator replicator, IList<Branch> branches,
                IList<XLinkData> xlinks, RepliKateParams rParams)
        {
            replicator.Replicate(
                branches, rParams.DstSrv, rParams.SrcSrv,
                Environment.CurrentDirectory, rParams.InitBranch, true);

            foreach (var xlink in xlinks)
            {
                replicator.Replicate(
                    xlink.pushBranches, xlink.DstSrv, xlink.SrcSrv,
                    Environment.CurrentDirectory, rParams.InitBranch, true);
            }
        }

        private static void DoAutomaticMerge(Replicator replicator, IDictionary<Branch, Changeset> mergeData, RepliKateParams rParams)
        {
            var mergeHandler = new MergeHandler(rParams.CmExec, rParams.StopOnError);
            mergeHandler.AutoMerge(mergeData, rParams.WkPath, rParams.DstSrv,
                Environment.CurrentDirectory, rParams.InitBranch);
        }

        private static string GetYesterdayDate()
        {
            DateTime yesterday = DateTime.Now.AddDays(-1);
            return yesterday.ToShortDateString();
        }

        enum ReplicateMode
        {
            None = 0,
            Pull = 1 << 0,
            Push = 1 << 1,
            Sync = Pull | Push,
        }

        private class RepliKateParams
        {
            public string SrcSrv;
            public string DstSrv;
            public string WkPath = "~/.replikate/wk";
            public string Syncdate = string.Empty;
            public string[] XLinks = new string[0];
            public int InitBranch = 0;
            public string CmExec = "cm";
            public bool StopOnError = true;
            public bool OnlyShowUnsynced = false;
            public ReplicateMode ReplicateMode = ReplicateMode.Pull;
        }

        private struct XLinkData
        { 
            public string repo;
            public string SrcSrv;
            public string DstSrv;
            public IList<Branch> pullBranches;
            public IList<Branch> pushBranches;
        }

    }
}
