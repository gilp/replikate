﻿using Codice.CmdRunner;

namespace repliKate
{
    public static class Cmd
    {
        private static string output;
        private static string error;

        public static string Output => output; 
        public static string Error => error;

        public static int Run(string command, string path)
        {
            Log.Debug($"{path}:$ {command}");

            output = error = null;
            int result = CmdRunner.ExecuteCommandWithResult(command, path, out output, out error, false);

            Log.Debug(output);

            if (!string.IsNullOrEmpty(error))
            {
                Log.Error(error);
            }

            return result;
        }
    }
}
