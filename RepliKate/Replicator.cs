﻿using System;
using System.Collections.Generic;

namespace repliKate
{
    internal class Replicator
    {
        private string mCmExec;
        private bool mStopOnError;

        public Replicator(string cmExec, bool stopOnError)
        {
            mCmExec = cmExec;
            mStopOnError = stopOnError;
        }

        internal void Replicate(
            IList<Branch> branches, string src, string dst,
            string wkpath, int initBranch, bool push = false)
        {
            int init = Environment.TickCount;

            string pushSwitch = (push ? " --push" : "");
            string cmd = $@"{mCmExec} replicate ""br:{{0}}@{src}"" ""rep:{dst}"" {pushSwitch}";

            int count = 0;

            foreach (Branch branch in branches)
            {
                if (count < initBranch)
                {
                    ++count;
                    continue;
                }

                var timespan = TimeSpan.FromMilliseconds(Environment.TickCount - init).ToString();
                Log.Info($"Replicating branch {branch.Name}. " +
                    $"{++count} of {branches.Count}... Time so far {timespan}");

                string command = string.Format(cmd, branch.Name);

                //replicate
                try
                {
                    int ini = Environment.TickCount;
                    int cmdresult = Cmd.Run(command, wkpath);

                    Log.Info($"Branch {branch.Name} replicated in {Environment.TickCount - ini} ms");

                    if (!(cmdresult == 0) && mStopOnError)
                    {
                        throw new Exception($"Replication didn't finished properly on branch {branch.Name}:{Environment.NewLine}{Cmd.Error}");
                    }

                }
                catch (Exception e)
                {
                    Log.Error($"Failed to replicate branch {branch.Name}." + 
                        $"{e.Message}{Environment.NewLine}at {e.StackTrace}");

                    if (mStopOnError)
                    {
                        throw e;
                    }
                    else
                        continue;
                }

                timespan = TimeSpan.FromMilliseconds(Environment.TickCount - init).ToString();
                Log.Info($"Done replicating branch {branch.Name}." + 
                    $"{++count} of {branches.Count}! Time so far {timespan}");
            }
        }

    }
}
