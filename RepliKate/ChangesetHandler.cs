﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace repliKate
{
    struct Changeset
    {
        internal long Id;
        internal string Guid;

        internal Changeset(string id, string guid)
        {
            Id = long.Parse(id);
            Guid = guid;
        }

        public override bool Equals(object obj)
        {
            return ((Changeset)obj).Guid == Guid;
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Id} - {Guid}";
        }
    }

    internal class ChangesetHandler
    {
        private string mSrcServer;
        private string mCmExec;
        private string mWkpath;

        public ChangesetHandler(string srcserver, string cmExec, string wkpath)
        {
            mSrcServer = srcserver;
            mCmExec = cmExec;
            mWkpath = wkpath;
        }

        internal IDictionary<Branch, IList<Changeset>> GetChangesets(IList<Branch> branches, string initialdate, IDictionary<Branch, IList<Changeset>> filter = null)
        {
            var result = new Dictionary<Branch, IList<Changeset>>();

            foreach (var branch in branches)
            {
                var changesetList = new List<Changeset>();

                try
                {
                    string dateFilter = "";
                    if (!string.IsNullOrEmpty(initialdate))
                    {
                        dateFilter = $"and date >= '{initialdate}'";
                    }

                    string cmd = $@"{mCmExec} find ""changeset where branch = '{branch.Name}' {dateFilter} " + 
                                 $@"on repository '{mSrcServer}'"" --format={{changesetid}}|{{guid}} --nototal";

                    Cmd.Run(cmd, mWkpath);
                    StringReader reader = new StringReader(Cmd.Output);

                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Trim() == string.Empty)
                            continue;

                        // output will be like 2625|17047DCC-C4D3-4057-8D1F-282AE76899CF
                        string[] values = line.Split('|');

                        Changeset cs = new Changeset(values[0], values[1]);

                        // filter changesets
                        if (filter != null && filter.ContainsKey(branch) && filter[branch].Contains(cs))
                        {
                            continue;
                        }

                        changesetList.Add(cs);
                    }

                    changesetList.Sort(CompareChangesets);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error listing changesets: {e.Message}");
                    Log.ErrorFormat($"Error listing changesets: {Environment.NewLine}" + 
                        $"{e.Message} at {Environment.NewLine}" + 
                        $"{e.StackTrace}");

                    return null;
                }

                if (changesetList.Count > 0)
                {
                    result.Add(branch, changesetList);
                }
            }

            return result;
        }

        internal IDictionary<Branch, Changeset> GetLatestChangeset(IDictionary<Branch, IList<Changeset>> changesetData)
        {
            var result = new Dictionary<Branch, Changeset>();

            foreach (var pair in changesetData)
            {
                if (pair.Value.Count == 0)
                {
                    continue;
                }

                result.Add(pair.Key, pair.Value.Last());
            }

            return result;
        }

        public int CompareChangesets(Changeset x, Changeset y)
        {
            return x.Id.CompareTo(y.Id);
        }


    }
}
